<?php 
	include("math.php");

  /* Command line script to solve binomial coefficient */

  $n = $argv[1];
  $k = $argv[2];
  if((isset($n) && $n >=0) && (isset($k) && $k >=0)){
    echo binomial($n, $k);
  }else{
    echo "Usage:
    php binomial.php integer integer
    
Example: 
    php binomial.php 2 4
";
  }
  
  
?>
