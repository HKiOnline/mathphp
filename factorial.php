<?php 
	include("math.php");

  /* Command line script to calculte factorials */

  $num = $argv[1];
  if(isset($num) && $num>=0){
    settype($num, "integer");
    echo factorial($num);
  }else{
    echo "Usage:
    php factorial.php integer
    
Example: 
    php factorial.php 2
";
  }
  
  
?>
