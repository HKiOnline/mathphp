# MathPHP

This repository contains few math-formulas (binomial and binomial distribution). It does not follow any library structure at the moment (and most likely never will).

## Binomial distribution

```
#!bash

Usage:
    php binDistribution.php n p k
    
n number of trials
p probability of event happening, 
k desired number of successes 
    
Example: 
    php binomial.php 10 0.25 4
```

## Binomial coefficient

```
#!bash

Usage:
    php binomial.php integer integer
    
Example: 
    php binomial.php 2 4

```

## Factorials

```
#!bash

Usage:
    php factorial.php integer
    
Example: 
    php factorial.php 2
```