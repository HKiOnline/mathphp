<?php
/*
  Factorial
  http://en.wikipedia.org/wiki/Factorial
*/
function factorial($num) { 
  $product  = 1;
  while($num > 1){
    $product = ($product * ($num * ($num - 1)));
    $num = $num - 2;
  }
  return $product;
} 


/*
  Binomial coefficient
  http://en.wikipedia.org/wiki/Binomial_coefficient

*/

function binomial ($n, $k) { 
  return factorial($n) / ((factorial($k) * factorial($n-$k))); 
} 

/*
  Binomial distribution
  http://en.wikipedia.org/wiki/Binomial_distribution

  n number of trials
  p probability of event happening, 
  k desired number of successes 
*/ 
function binDistribution ($n, $p, $k) { 
  return binomial($n, $k) * pow($p, $k) * pow(1-$p, $n-$k); 
} 

?>