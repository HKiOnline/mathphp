<?php 
	include("math.php");
  
  /* Command line script to calculte binomial distribution */

  $n = $argv[1];
  $p = $argv[2];
  $k = $argv[3];
  if((isset($n) && $n >=0) && (isset($p) && $p >=0) && (isset($k) && $k >=0)){
    echo binDistribution($n, $p, $k);
  }else{
    echo "Usage:
    php binDistribution.php n p k
    
n number of trials
p probability of event happening, 
k desired number of successes 
    
Example: 
    php binomial.php 10 0.25 4
";
  }
  
  
?>
